﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Cites2019.DATA;
using Cites2019.Models.CITES;

namespace Cites2019.Controllers
{
    public class CitesAdminsController : Controller
    {
        private CitesContext db = new CitesContext();

        // GET: CitesAdmins
        public ActionResult Index()
        {
            return View(db.CitesAdmins.ToList());
        }

        // GET: CitesAdmins/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CitesAdmin citesAdmin = db.CitesAdmins.Find(id);
            if (citesAdmin == null)
            {
                return HttpNotFound();
            }
            return View(citesAdmin);
        }

        // GET: CitesAdmins/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CitesAdmins/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UserId,FirstName,LastName,Title,Region,Role,Date_Entered")] CitesAdmin citesAdmin)
        {
            if (ModelState.IsValid)
            {
                db.CitesAdmins.Add(citesAdmin);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(citesAdmin);
        }

        // GET: CitesAdmins/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CitesAdmin citesAdmin = db.CitesAdmins.Find(id);
            if (citesAdmin == null)
            {
                return HttpNotFound();
            }
            return View(citesAdmin);
        }

        // POST: CitesAdmins/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UserId,FirstName,LastName,Title,Region,Role,Date_Entered")] CitesAdmin citesAdmin)
        {
            if (ModelState.IsValid)
            {
                db.Entry(citesAdmin).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(citesAdmin);
        }

        // GET: CitesAdmins/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CitesAdmin citesAdmin = db.CitesAdmins.Find(id);
            if (citesAdmin == null)
            {
                return HttpNotFound();
            }
            return View(citesAdmin);
        }

        // POST: CitesAdmins/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CitesAdmin citesAdmin = db.CitesAdmins.Find(id);
            db.CitesAdmins.Remove(citesAdmin);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
