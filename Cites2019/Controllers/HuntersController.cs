﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Cites2019.DATA;
using Cites2019.Models.CITES;

namespace Cites2019.Controllers
{
    public class HuntersController : Controller
    {
        private CitesContext db = new CitesContext();

        // GET: Hunters
        public ActionResult Index()
        {
            return View(db.Hunters.ToList());
        }

        // GET: Hunters/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hunter hunter = db.Hunters.Find(id);
            if (hunter == null)
            {
                return HttpNotFound();
            }
            return View(hunter);
        }

        // GET: Hunters/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Hunters/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "HunterID,TwraId,H_FirstName,H_LastName,H_Address1,H_Address2,H_City,H_State,H_Zip,H_landowner")] Hunter hunter)
        {
            if (ModelState.IsValid)
            {
                db.Hunters.Add(hunter);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(hunter);
        }

        // GET: Hunters/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hunter hunter = db.Hunters.Find(id);
            if (hunter == null)
            {
                return HttpNotFound();
            }
            return View(hunter);
        }

        // POST: Hunters/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "HunterID,TwraId,H_FirstName,H_LastName,H_Address1,H_Address2,H_City,H_State,H_Zip,H_landowner")] Hunter hunter)
        {
            if (ModelState.IsValid)
            {
                db.Entry(hunter).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(hunter);
        }

        // GET: Hunters/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hunter hunter = db.Hunters.Find(id);
            if (hunter == null)
            {
                return HttpNotFound();
            }
            return View(hunter);
        }

        // POST: Hunters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Hunter hunter = db.Hunters.Find(id);
            db.Hunters.Remove(hunter);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
