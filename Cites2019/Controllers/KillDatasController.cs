﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Cites2019.DATA;
using Cites2019.Models.CITES;

namespace Cites2019.Controllers
{
    public class KillDatasController : Controller
    {
        private CitesContext db = new CitesContext();

        // GET: KillDatas
        public ActionResult Index()
        {
            return View(db.KillDatas.ToList());
        }

        // GET: KillDatas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KillData killData = db.KillDatas.Find(id);
            if (killData == null)
            {
                return HttpNotFound();
            }
            return View(killData);
        }

        // GET: KillDatas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: KillDatas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "HD_ID,Kill_Date,KillSex,HrvstCounty,Species,Reg,Kill_method,Weight,Hunter_Id,TagNumber")] KillData killData)
        {
            if (ModelState.IsValid)
            {
                db.KillDatas.Add(killData);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(killData);
        }

        // GET: KillDatas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KillData killData = db.KillDatas.Find(id);
            if (killData == null)
            {
                return HttpNotFound();
            }
            return View(killData);
        }

        // POST: KillDatas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "HD_ID,Kill_Date,KillSex,HrvstCounty,Species,Reg,Kill_method,Weight,Hunter_Id,TagNumber")] KillData killData)
        {
            if (ModelState.IsValid)
            {
                db.Entry(killData).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(killData);
        }

        // GET: KillDatas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KillData killData = db.KillDatas.Find(id);
            if (killData == null)
            {
                return HttpNotFound();
            }
            return View(killData);
        }

        // POST: KillDatas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            KillData killData = db.KillDatas.Find(id);
            db.KillDatas.Remove(killData);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
