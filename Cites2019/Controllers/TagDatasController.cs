﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Cites2019.DATA;
using Cites2019.Models.CITES;

namespace Cites2019.Controllers
{
    public class TagDatasController : Controller
    {
        private CitesContext db = new CitesContext();

        // GET: TagDatas
        public ActionResult Index()
        {
            return View(db.TagDatas.ToList());
        }

        // GET: TagDatas/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TagData tagData = db.TagDatas.Find(id);
            if (tagData == null)
            {
                return HttpNotFound();
            }
            return View(tagData);
        }

        // GET: TagDatas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TagDatas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TagNumber,T_Region,T_Species,T_Officer,T_Admin,Tdate,Status")] TagData tagData)
        {
            if (ModelState.IsValid)
            {
                db.TagDatas.Add(tagData);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tagData);
        }

        // GET: TagDatas/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TagData tagData = db.TagDatas.Find(id);
            if (tagData == null)
            {
                return HttpNotFound();
            }
            return View(tagData);
        }

        // POST: TagDatas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TagNumber,T_Region,T_Species,T_Officer,T_Admin,Tdate,Status")] TagData tagData)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tagData).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tagData);
        }

        // GET: TagDatas/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TagData tagData = db.TagDatas.Find(id);
            if (tagData == null)
            {
                return HttpNotFound();
            }
            return View(tagData);
        }

        // POST: TagDatas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            TagData tagData = db.TagDatas.Find(id);
            db.TagDatas.Remove(tagData);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
