namespace Cites2019.Migrations.CITES
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CitesAdmins",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        FirstName = c.String(maxLength: 25),
                        LastName = c.String(nullable: false, maxLength: 25),
                        Title = c.String(nullable: false, maxLength: 30),
                        Region = c.String(nullable: false, maxLength: 2),
                        Role = c.String(nullable: false, maxLength: 25),
                        Date_Entered = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.Counties",
                c => new
                    {
                        C_Id = c.Int(nullable: false, identity: true),
                        CountyId = c.String(maxLength: 4),
                        CountyName = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.C_Id);
            
            CreateTable(
                "dbo.Hunters",
                c => new
                    {
                        HunterID = c.Int(nullable: false, identity: true),
                        TwraId = c.String(maxLength: 50),
                        H_FirstName = c.String(nullable: false, maxLength: 50),
                        H_LastName = c.String(nullable: false, maxLength: 50),
                        H_Address1 = c.String(nullable: false, maxLength: 50),
                        H_Address2 = c.String(maxLength: 50),
                        H_City = c.String(maxLength: 50),
                        H_State = c.String(maxLength: 50),
                        H_Zip = c.String(maxLength: 25),
                        H_landowner = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.HunterID);
            
            CreateTable(
                "dbo.KillDatas",
                c => new
                    {
                        HD_ID = c.Int(nullable: false, identity: true),
                        Kill_Date = c.DateTime(nullable: false),
                        KillSex = c.String(maxLength: 50),
                        HrvstCounty = c.String(nullable: false, maxLength: 50),
                        Species = c.String(maxLength: 50),
                        Reg = c.String(nullable: false, maxLength: 50),
                        Kill_method = c.String(maxLength: 50),
                        Weight = c.String(maxLength: 50),
                        Hunter_Id = c.String(maxLength: 50),
                        TagNumber = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.HD_ID);
            
            CreateTable(
                "dbo.TagDatas",
                c => new
                    {
                        TagNumber = c.String(nullable: false, maxLength: 10),
                        T_Region = c.String(nullable: false, maxLength: 5),
                        T_Species = c.String(nullable: false, maxLength: 10),
                        T_Officer = c.String(nullable: false, maxLength: 50),
                        T_Admin = c.String(nullable: false, maxLength: 50),
                        Tdate = c.DateTime(nullable: false),
                        Status = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.TagNumber);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TagDatas");
            DropTable("dbo.KillDatas");
            DropTable("dbo.Hunters");
            DropTable("dbo.Counties");
            DropTable("dbo.CitesAdmins");
        }
    }
}
