namespace Cites2019.Migrations.CITES
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRegiontoCounty : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Counties", "Region", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Counties", "Region");
        }
    }
}
