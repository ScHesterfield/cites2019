// <auto-generated />
namespace Cites2019.Migrations.CITES
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class AddRegiontoCounty : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddRegiontoCounty));
        
        string IMigrationMetadata.Id
        {
            get { return "201909181405155_Add Region to County"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
