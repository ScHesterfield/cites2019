namespace Cites2019.Migrations.Identity
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Cites2019.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;

    internal sealed class Configuration : DbMigrationsConfiguration<Cites2019.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"Migrations\Identity";
        }

        protected override void Seed(ApplicationDbContext context)
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            if (!roleManager.RoleExists("Admin"))
                roleManager.Create(new IdentityRole("Admin"));

            if (!roleManager.RoleExists("Guest"))
                roleManager.Create(new IdentityRole("Guest"));

            if (!roleManager.RoleExists("Officer"))
                roleManager.Create(new IdentityRole("Officer"));


            var userManager = new UserManager<ApplicationUser>(new
            UserStore<ApplicationUser>(context));



            if (userManager.FindByEmail("sandra.chesterfield@tn.gov") == null)
            {
                var user = new ApplicationUser
                {
                    Email = "sandra.chesterfield@tn.gov",
                    UserName = "sandra.chesterfield@tn.gov",
                };
                var result = userManager.Create(user, "P@$$w0rd");
                if (result.Succeeded)
                    userManager.AddToRole(userManager.FindByEmail(user.Email).Id,
                 "Admin");
            }
            if (userManager.FindByEmail("roxanna.bradley@tn.gov") == null)
            {
                var user = new ApplicationUser
                {
                    Email = "roxanna.bradley@tn.gov",
                    UserName = "roxanna.bradley@tn.gov",
                };
                var result = userManager.Create(user, "P@$$w0rd");
                if (result.Succeeded)
                    userManager.AddToRole(userManager.FindByEmail(user.Email).Id, "Guest");
            }
              if (userManager.FindByEmail("Todd.Allen@tn.gov") == null)
            {
                var user = new ApplicationUser
                {
                    Email = "Todd.Allen@tn.gov",
                    UserName = "Todd.Allen@tn.gov",
                };
                var result = userManager.Create(user, "P@$$w0rd");
                if (result.Succeeded)
                    userManager.AddToRole(userManager.FindByEmail(user.Email).Id, "Officer");
            }
        }
            

        }
    }
