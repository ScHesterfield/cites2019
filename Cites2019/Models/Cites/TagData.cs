﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Cites2019.Models.CITES
{
    public class TagData
    {
        [Key]
        [Required]
        [Display(Name = "TAG NUMBER")]
        [StringLength(10)]
        public string TagNumber { get; set; }

        [Required(ErrorMessage = "Region is Required")]
        [Display(Name = "REGION")]
        [StringLength(5)]
        public string T_Region { get; set; }

        [Required]
        [Display(Name = "SPECIES")]
        [StringLength(10)]
        public string T_Species { get; set; }


        [Required]
        [Display(Name = "ISSUED TO:")]
        [StringLength(50)]
        public string T_Officer { get; set; }

        [Required(ErrorMessage = "Please enter Issuer's Name:")]
        [Display(Name = "ADMIN ISSUER")]
        [StringLength(50)]
        public string T_Admin { get; set; }


        [Display(Name = "Date Entered:")]
        [Required(ErrorMessage = "Please enter date assigned to User")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Tdate { get; set; }


        [Display(Name = "Tag Status:")]
        [StringLength(50)]
        public string Status { get; set; }

    }
}