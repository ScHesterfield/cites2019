﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Cites2019.Models.CITES
{
    public class CitesAdmin
    {        [Key]
        public int UserId { get; set; }

        [Display(Name = "First Name")]
        [StringLength(25)]
        public string FirstName { get; set; }
        [Required(ErrorMessage = " Required-First Name ")]

        [Display(Name = "Last Name")]
        [StringLength(25)]
        public string LastName { get; set; }
        [Required(ErrorMessage = " Required-Last Name")]

        [Display(Name = "Title")]
        [StringLength(30)]
        public string Title { get; set; }
        [Required(ErrorMessage = " Required-Title")]

        [Display(Name = "Region")]
        [StringLength(2)]
        public string Region { get; set; }
        [Required(ErrorMessage = " Required-Region")]

        [Display(Name = "User Role")]
        [StringLength(25)]
        public string Role { get; set; }

        [Display(Name = "Date Entered:")]
        [Required(ErrorMessage = "Please enter today's date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Date_Entered { get; set; }

    }
}