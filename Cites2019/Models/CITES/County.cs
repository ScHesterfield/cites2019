﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Cites2019.Models.CITES
{
    public class County
    {
        [Key]
        public int C_Id { get; set; }
        [StringLength(4)]
        public string CountyId { get; set; }
        [StringLength(50)]
        public string CountyName { get; set; }
        public string Region { get; set; }
    }
}