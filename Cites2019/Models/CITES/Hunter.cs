﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Cites2019.Models.CITES
{
    public class Hunter
    {
        [Key]
        [Display(Name = "Hunter ID")]
        public int HunterID { get; set; }


        [Display(Name = "TWRA_ID")]
        [StringLength(50)]
        public string TwraId { get; set; }

        [Required]
        [Display(Name = "First Name")]
        [StringLength(50)]
        public string H_FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        [StringLength(50)]
        public string H_LastName { get; set; }

        [Required]
        [Display(Name = "Address")]
        [StringLength(50)]
        public string H_Address1 { get; set; }


        [Display(Name = "Address2")]
        [StringLength(50)]
        public string H_Address2 { get; set; }


        [Display(Name = "City")]
        [StringLength(50)]
        public string H_City { get; set; }


        [Display(Name = "State")]
        [StringLength(50)]
        public string H_State { get; set; }


        [Display(Name = "Zip Code")]
        [StringLength(25)]
        [DataType(DataType.PostalCode)]
        public string H_Zip { get; set; }

        [Display(Name = "Landowner? (YES/NO)")]
        [StringLength(50)]
        public string H_landowner { get; set; }

    }
}