﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Cites2019.Models.CITES
{
    public class KillData
    {
        [Key]
        public int HD_ID { get; set; }


        [Display(Name = "KILL DATE:")]
        [Required(ErrorMessage = "Please enter the Kill date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Kill_Date { get; set; }


        [Display(Name = "KILL SEX:")]
        [StringLength(50)]
        public string KillSex { get; set; }

        [Required]
        [Display(Name = "COUNTY:")]
        [StringLength(50)]
        public string HrvstCounty { get; set; }

        [Display(Name = "SPECIES:")]
        [StringLength(50)]
        public string Species { get; set; }

        [Required]
        [Display(Name = "REGION:(1-4)")]
        [Range(1, 4, ErrorMessage = "Region only [1-4]")]
        [StringLength(50)]
        public string Reg { get; set; }

        [Display(Name = "KILL METHOD:")]
        [StringLength(50)]
        public string Kill_method { get; set; }

        [Display(Name = "Weight:")]
        [StringLength(50)]
        public string Weight { get; set; }

        [Display(Name = "Hunter Id:")]
        [StringLength(50)]
        public string Hunter_Id { get; set; }

        [Display(Name = "Tag Number:")]
        [StringLength(50)]
        public string TagNumber { get; set; }

    }
}