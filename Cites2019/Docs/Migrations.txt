﻿Enable-Migrations -ContextTypeName CitesContext -MigrationsDirectory Migrations\CITES            (can only be executed once)
add-migration -ConfigurationTypeName Cites2019.Migrations.CITES.Configuration "InitialCreate"            (execute this whenever theres a model change)
Update-database -ConfigurationTypeName Cites2019.Migrations.CITES.Configuration                    (can be executed anytime)

==============================================ApplicationDbContext================================================================================================================

Enable-Migrations -ContextTypeName ApplicationDbContext -MigrationsDirectory Migrations\Identity           (can only be executed once)
add-migration -ConfigurationTypeName Cites2019.Migrations.Identity.Configuration "InitialIdentity"  
Update-database -ConfigurationTypeName Cites2019.Migrations.Identity.Configuration                           (can be executed anytime)

