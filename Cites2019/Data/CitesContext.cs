﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Cites2019.Models.CITES;

namespace Cites2019.DATA
{
    public class CitesContext : DbContext
    {
        public CitesContext() : base("DefaultConnection")
        { }
        public DbSet<CitesAdmin> CitesAdmins { get; set; }
        public DbSet<KillData> KillDatas { get; set; }
        public DbSet<Hunter> Hunters { get; set; }
        public DbSet<TagData> TagDatas { get; set; }
        public DbSet<County> Counties { get; set; }

    }
}